# Tigress [![Build Status](https://travis-ci.org/anywhere-com/tigress.js.svg?branch=master)](https://travis-ci.org/anywhere-com/tigress.js)

Tigress is a small library to provide a strongly typed state container to be used together with [TypeScript](https://www.typescriptlang.org/) and immutable data structures.

The library is split up into [tigress](packages/tigress/README.md) and [tigress-react](packages/tigress-react/README.md).
