# Contributing to Tigress

## Deploying

Tigress is deployed with `lerna` and `yarn`. Before deploying, you must be logged into npm using `npm adduser` with an account that has push priveleges to this repository.
Once authentication is setup, the basic workflow for deploying is to run:

```sh
yarn
yarn test
./node_modules/.bin/lerna publish
```

