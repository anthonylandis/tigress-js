import * as React from 'react';
import * as ReactDOM from 'react';
import {Component} from 'react';
import {reactive, componentListMapReaction} from '../src/tigress-react';
import {IRef, IRefResolved} from 'tigress';
import {rootRef} from 'tigress';
import * as testRenderer from 'react-test-renderer';
import * as Immutable from 'immutable';

@reactive
class TestComponent extends React.Component<{theRef: IRef<any>}, undefined> {
  render() {
    return <div>{this.props.theRef.get()}</div>;
  }
}

test('reactive components work', () => {
  const r0 = rootRef(0);
  const component = testRenderer.create(
      <TestComponent theRef={r0} />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
  r0.reset(1);
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('componentListMapReaction works', () => {
  const r0: IRefResolved<Immutable.List<number>> = rootRef(Immutable.List([0, 1, 2]));
  const component = testRenderer.create(
      <TestComponent theRef={componentListMapReaction(r0, (r, key) => <TestComponent key={key} theRef={r} />)} />
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
