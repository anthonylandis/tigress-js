/* tslint:disable */
import * as React from 'react';
import { reaction, Reaction, shallowEquals, listMapReaction, IObserver } from 'tigress';
import { IRefRO, ISubscription } from 'tigress';
import * as Immutable from 'immutable';

export type AnimationFunction = (timestamp: any) => void;

let _animating: boolean = false;
let _animationRequested: boolean = false;
let _animationFrameTime: any = null;
let _animationQueue: AnimationFunction[] = [];

export function scheduleAnimationFrame(f: AnimationFunction): void {
    if (_animating === true) {
        f(_animationFrameTime);
    } else {
        _animationQueue.push(f);
        if (_animationRequested !== true) {
            _animationRequested = true;
            window.requestAnimationFrame(function(timestamp: any) {
                _animating = true;
                _animationRequested = false;
                const queue = _animationQueue;
                _animationQueue = [];
                _animationFrameTime = timestamp;
                const len = queue.length;
                for (let i = 0; i < len; ++i) {
                    try {
                        queue[i](timestamp);
                    } catch (e) {
                        /* tslint:disable:no-console */
                        console.error(e);
                        /* tslint:enable:no-console */
                    }
                }
                _animating = false;
            });
        }
    }
}

export function debounceScheduler(scheduler: (f: () => void) => void,
                                  reaction: Reaction<any>, f: () => void): ISubscription {
    let running = true;
    let sub: null | ISubscription = null;
    const observer: IObserver<any> = {
        start: () => { },
        error: () => { },
        complete: () => { },
        next: (_: any) => {
            if (sub) sub.unsubscribe();
            scheduler(function() {
                if (running) {
                    sub = reaction.subscribeInvalidated(observer);
                    f();
                }
            });
        },
    };
    sub = reaction.subscribeInvalidated(observer);
    return {
        unsubscribe: () => {
            running = false;
            if (sub) sub.unsubscribe();
        },
        closed: () => running
    };
}

export function debounceComputeAnimationFrame(reaction: Reaction<any>, f: () => void): ISubscription {
    return debounceScheduler(scheduleAnimationFrame, reaction, f);
}

export function scheduleImmediate(reaction: Reaction<any>, f: () => void): ISubscription {
    return debounceScheduler((g) => g(), reaction, f);
}

export function reactive<T extends { new (...args: any[]): React.Component<any, any> }>(cls: T): T {
    const { render, componentWillMount, shouldComponentUpdate, componentWillUnmount } = cls.prototype;
    return class extends cls {
        private _reaction: Reaction<any>;
        private _sub?: ISubscription;

        render() {
            return this._reaction.getNonReactive();
        }

        componentWillMount() {
            if (componentWillMount) componentWillMount.call(this);
            const This = this;
            const r = reaction(function() {
                return render.call(This);
            });
            this._reaction = r;
            this._sub = scheduleImmediate(r, function() {
                This.forceUpdate();
            });
        }

        shouldComponentUpdate(nextProps: any, nextState: any) {
            if (!shallowEquals(this.props, nextProps)) {
                if(this._reaction) this._reaction.invalidate();
                return true;
            }
            if (!shallowEquals(this.state, nextState)) {
                if(this._reaction) this._reaction.invalidate();
                return true;
            }
            if (shouldComponentUpdate) {
                return shouldComponentUpdate.apply(this, arguments);
            }
            return false;
        }

        componentWillUnmount() {
            if (componentWillUnmount) componentWillUnmount.call(this);
            if(this._sub) this._sub.unsubscribe();
            if(this._reaction) this._reaction.dispose();
        }
    };
}

export function componentListMapReaction<T>(cur: IRefRO<Immutable.List<T>>,
                                            f: (cur: IRefRO<T>, key: string) => React.ReactElement<any>):
    IRefRO<Array<React.ReactElement<any>>> {
    return listMapReaction(cur, function(c) {
        const prop = c.prop();
        const name = prop == null ? "" : prop.name;
        return f(c, name);
    });
}
