import * as Immutable from "immutable";
import { IObserver } from "./IObserver";
import { IPropRO } from "./IProp";
import { IRefRO } from "./IRef";
import { ISubscription } from "./ISubscription";
import { SubscriptionManager } from "./SubscriptionManager";

export class RefSubscriptionManager<P, V> extends SubscriptionManager<V> {
    private _parentRef: IRefRO<P>;
    private _prop: IPropRO<P, V>;
    private _parentSub: ISubscription | null;

    constructor(parentRef: IRefRO<P>, prop: IPropRO<P, V>) {
        super();
        this._parentRef = parentRef;
        this._prop = prop;
    }

    subscribe(observer: IObserver<V>): ISubscription {
        if (this._parentSub == null) this._subscribeParent();
        return super.subscribe(observer);
    }

    protected _onUnsubscribe(): void {
        if (this.isEmpty()) {
            const sub = this._parentSub;
            if (sub != null) {
                sub.unsubscribe();
                this._parentSub = null;
            }
        }
    }

    private _subscribeParent() {
        const This = this;
        const prop = this._prop;
        let oldV: V | null = null;
        this._parentSub = this._parentRef.subscribe({
            start: (sub) => { },
            complete: () => { },
            error: () => { },
            next: (newP) => {
                if (This.isEmpty())
                    return;
                const newV = prop.get(newP);
                if (!Immutable.is(oldV, newV)) {
                    oldV = newV;
                    This.next(newV);
                }
            },
        });
    }
}
