import { IProp, IPropRecord } from "./IProp";
import { IRef, IRefResolved } from "./IRef";
import { RefResolvedRO, RefRO } from "./RefRO";

export class Ref<P, V> extends RefRO<P, V> implements IRef<V> {
    _parent: IRef<P>;
    _prop: IProp<P, V>;

    constructor(parent: IRef<P>, prop: IProp<P, V>) {
        super(parent, prop);
        this._parent = parent;
        this._prop = prop;
    }

    swap(updateFn: (curVal: V | null) => V | null): V | null {
        const prop = this._prop;
        const newP = this._parent.swap((pVal) => {
            const curVal = prop.get(pVal);
            return prop.set(pVal, updateFn(curVal));
        });
        return prop.get(newP);
    }

    reset(newVal: V): V | null {
        return this.swap((_) => newVal);
    }

    child<X>(prop: IProp<V, X>): Ref<V, X> {
        return new Ref(this, prop);
    }

    resolve(): IRefResolved<V> | null {
        const cur = this.get();
        if (cur != null) {
            return new RefResolved(cur, this);
        } else return null;
    }

    equals(other: any) {
        return super.equals(other);
    }
}

export class RefResolved<T> extends RefResolvedRO<T> implements IRefResolved<T> {
    _ref: IRef<T>;

    constructor(init: T, ref: IRef<T>) {
        super(init, ref);
        this._ref = ref;
    }

    swap(updateFn: (curVal: T | null) => T | null): T | null {
        return this._ref.swap(updateFn);
    }

    reset(newVal: T): T | null {
        return this.swap((_) => newVal);
    }

    swapResolved(updateFn: (x: T) => T): T {
        const newVal = this.swap((curVal) => {
            if (curVal == null) return null;
            return updateFn(curVal);
        });
        if (newVal == null) return this._last;
        this._last = newVal;
        return newVal;
    }

    child<V>(prop: IProp<T, V>): IRef<V> {
        return this._ref.child(prop);
    }

    resolve() {
        return this;
    }

    childRecord<V>(prop: IPropRecord<T, V>): IRefResolved<V> {
        return new RefResolved(prop.getField(this.getResolved()), this.child(prop));
    }
}
