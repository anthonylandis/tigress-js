import { IEquals } from "./equals";
import { IObservable } from "./IObservable";
import { IObserver } from "./IObserver";
import { IProp, IPropRecord, IPropRecordRO, IPropRO } from "./IProp";
import { ISubscription } from "./ISubscription";
import { MetadataMap } from "./metadata";
import { Metadata } from "./metadata";

export interface IRefRO<T> extends IObservable<T>, IEquals {
    getNonReactive(): T | null;
    /** Indicates whether an actual value is available yet for Promises and other async or lazy computations */
    realized(): boolean;
    metadataStore(): IRef<MetadataMap>;
    cleanCopy(): IRefRO<T>;
    get(): T | null;
    childRO<V>(prop: IPropRO<T, V>): IRefRO<V>;
    /** Returns the Prop that was used to derive this ref or null if this is a root Ref */
    prop(): IPropRO<any, T> | null;
    resolveRO(): IRefResolvedRO<T> | null;
    subscribeInvalidated(observer: IObserver<any>): ISubscription;
    metadata(): IRef<Metadata>;
    dirty(): IRefRO<boolean>;
}

export interface IRefResolvedRO<T extends {}> extends IRefRO<T> {
    getResolved(): T;
    getResolvedNonReactive(): T;
    childRecordRO<V>(prop: IPropRecordRO<T, V>): IRefResolvedRO<V>;
}

export interface IRef<T> extends IRefRO<T> {
    child<V>(prop: IProp<T, V>): IRef<V>;
    resolve(): IRefResolved<T> | null;
    swap(updateFn: (curVal: T | null) => T | null): T | null;
    reset(newVal: T): T | null;
    // TODO element<V>(prop: IItemPrism<T,V>) : IItemRef<V>;
}

export interface IRefResolved<T extends {}> extends IRef<T>, IRefResolvedRO<T> {
    childRecord<A>(prop: IPropRecord<T, A>): IRefResolved<A>;
    swapResolved(updateFn: (x: T) => T): T;
}
