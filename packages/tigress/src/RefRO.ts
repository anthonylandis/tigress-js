import * as Immutable from "immutable";
import { Equals } from "./equals";
import { IObserver } from "./IObserver";
import { IProp, IPropRecordRO, IPropRO } from "./IProp";
import { IRef, IRefResolvedRO, IRefRO } from "./IRef";
import { ISubscription } from "./ISubscription";
import { Metadata, MetadataMap } from "./metadata";
import { nullRef } from "./nullRef";
import { Prop } from "./Prop";
import { RefSubscriptionManager } from "./RefSubscriptionManager";
import { ReactiveContext, registerDep } from "./registerDep";
import { SubscriptionManager } from "./SubscriptionManager";

export class RefRO<P, V> extends Equals implements IRefRO<V> {
    _ro_parent: IRefRO<P>;
    _ro_prop: IPropRO<P, V>;
    _subMgr?: RefSubscriptionManager<P, V>;
    _metadataStore?: IRef<MetadataMap>;
    _metadataRef?: IRef<Metadata>;
    _cleanCopyRef?: IRefRO<V>;
    _dirtyRef?: IRefRO<boolean>;

    constructor(parent: IRefRO<P>, prop: IPropRO<P, V>) {
        super();
        this._ro_parent = parent;
        this._ro_prop = prop;
    }

    _getSubMgr(): RefSubscriptionManager<P, V> {
        let subMgr = this._subMgr;
        if (subMgr == null) {
            subMgr = new RefSubscriptionManager(this._ro_parent, this._ro_prop);
            this._subMgr = subMgr;
        }
        return subMgr;
    }

    subscribe(observer: IObserver<V>) {
        return this._getSubMgr().subscribe(observer);
    }

    subscribeInvalidated(observer: IObserver<any>) {
        return this._getSubMgr().subscribe(observer);
    }

    getNonReactive() {
        const p = this._ro_parent.getNonReactive();
        if (p == null) return null;
        return this._ro_prop.get(p);
    }

    get() {
        registerDep(this);
        return this.getNonReactive();
    }

    realized(): boolean {
        return this._ro_parent.realized();
    }

    metadata(): IRef<Metadata> {
        if (this._metadataRef == null)
            this._metadataRef = _metadata(this.metadataStore());
        return this._metadataRef;
    }

    metadataStore(): IRef<MetadataMap> {
        if (this._metadataStore == null)
            this._metadataStore = _subMetadata(this._ro_parent.metadataStore(), this._ro_prop);
        return this._metadataStore;
    }

    cleanCopy(): IRefRO<V> {
        if (this._cleanCopyRef == null)
            this._cleanCopyRef = this._ro_parent.cleanCopy().childRO(this._ro_prop);
        return this._cleanCopyRef;
    }

    dirty(): IRefRO<boolean> {
        if (this._dirtyRef == null)
            this._dirtyRef = _dirtyReaction(this, this._cleanCopyRef);
        return this._dirtyRef;
    }

    childRO<X>(prop: IPropRO<V, X>): IRefRO<X> {
        return new RefRO(this, prop);
    }

    prop() {
        return this._ro_prop;
    }

    resolveRO(): IRefResolvedRO<V> | null {
        const cur = this.get();
        if (cur != null) {
            return new RefResolvedRO(cur, this);
        } else return null;
    }

    equals(other: any) {
        return super.equals(other)
            && this._ro_parent.equals(other._ro_parent)
            && this._ro_prop.equals(other._ro_prop);
    }
}

export class RefResolvedRO<A> extends Equals implements IRefResolvedRO<A> {
    _last: A;
    _ro_ref: IRefRO<A>;

    constructor(init: A, ref: IRefRO<A>) {
        super();
        this._last = init;
        this._ro_ref = ref;
    }

    equals(other: any) {
        return super.equals(other) &&
            this._ro_ref === other._ro_ref;
    }

    getResolved() {
        registerDep(this);
        return this.getResolvedNonReactive();
    }

    getResolvedNonReactive() {
        const last = this._ro_ref.getNonReactive();
        if (last != null) {
            this._last = last;
            return last;
        }
        return this._last;
    }

    get() {
        return this._ro_ref.get();
    }

    getNonReactive() {
        return this._ro_ref.getNonReactive();
    }

    childRO<C>(prop: IPropRO<A, C>): IRefRO<C> {
        return this._ro_ref.childRO(prop);
    }

    childRecordRO<C>(prop: IPropRecordRO<A, C>): IRefResolvedRO<C> {
        return new RefResolvedRO(prop.getField(this.getResolved()), this.childRO(prop));
    }

    metadata(): IRef<Metadata> {
        return this._ro_ref.metadata();
    }

    metadataStore(): IRef<MetadataMap> {
        return this._ro_ref.metadataStore();
    }

    cleanCopy() {
        return this._ro_ref.cleanCopy();
    }

    dirty(): IRefRO<boolean> {
        return this._ro_ref.dirty();
    }

    prop() {
        return this._ro_ref.prop();
    }

    realized() {
        return this._ro_ref.realized();
    }

    resolveRO() {
        return this;
    }

    subscribe(observer: IObserver<A>) {
        return this._ro_ref.subscribe(observer);
    }

    subscribeInvalidated(observer: IObserver<any>) {
        return this._ro_ref.subscribeInvalidated(observer);
    }
}

export function _metadata(msgStore: IRef<MetadataMap>): IRef<Metadata> {
    const prop: IProp<MetadataMap, Metadata> = new Prop(
        function(m: MetadataMap) {
            return m.metadata || new Metadata();
        },
        function(m: MetadataMap, v: Metadata | null): MetadataMap {
            // if(m == null) return null;
            if (v == null) return { branches: m.branches };
            return { metadata: v, branches: m.branches };
        },
        "metadata",
    );
    return msgStore.child(prop);
}

function _subMetadata<S, A>(msgStore: IRef<MetadataMap>, prop: IPropRO<S, A>): IRef<MetadataMap> {
    const name = prop.name;
    return msgStore.child(new Prop(
        function(m: MetadataMap) {
            if (m == null) return null;
            const mBranches = m.branches;
            if (mBranches == null) return {};
            return mBranches.get(name) || {};
        },
        function(m: MetadataMap, v: MetadataMap | null): MetadataMap {
            // if(m == null) return null;
            if (v == null) return m;
            const mBranches = m.branches || Immutable.Map<string, any>();
            const mBranches2 = mBranches.set(name, v);
            return { metadata: m.metadata, branches: mBranches2 };
        },
        "branches." + name));
}

export function _dirtyReaction<A>(ref: IRefRO<A>, clean?: IRefRO<A>): IRefRO<boolean> {
    return reaction(function() {
        if (clean == null) return false;
        const cleanVal = clean.get();
        const curVal = ref.get();
        return shallowEquals(cleanVal, curVal) ? false : true;
    });
}

export function shallowEquals(x: any, y: any): boolean {
    if (x === y) return true;
    if (x.equals) return x.equals(y);
    if (typeof x !== "object" || x === null ||
        typeof y !== "object" || y === null) {
        return false;
    }
    return false;
}

export class Reaction<T> extends Equals implements IRefRO<T> {
    _f: () => T;
    _value?: T;
    _dirty: boolean;
    _subs: ISubscription[];
    _changeMgr?: SubscriptionManager<T>;
    _invalidationMgr?: SubscriptionManager<null>;

    constructor(f: () => T) {
        super();
        this._f = f;
        this._dirty = true;
        this._subs = [];
    }

    _registerDep(ref: IRefRO<any>) {
        const This = this;
        const sub = ref.subscribeInvalidated({
            start: () => {},
            next: (_) => {
                This.invalidate();
                sub.unsubscribe();
            },
            error: () => {},
            complete: () => { },
        });
        this._subs.push(sub);
    }

    invalidate(): void {
        const wasDirty = this._dirty;
        if (!wasDirty) {
            this._dirty = true;
            const iMgr = this._invalidationMgr;
            if (iMgr != null) {
                iMgr.next(null);
            }
        }
        const chMgr = this._changeMgr;
        if (chMgr != null && !chMgr.isEmpty()) {
            this.compute();
        }
    }

    compute(): T {
        const curScope = ReactiveContext.__reactiveListener;
        try {
            ReactiveContext.__reactiveListener = (r) => this._registerDep(r);
            const curVal = this._value;
            this._dirty = false;
            this._unregisterRefs();
            const newVal = this._f();
            this._value = newVal;
            const chMgr = this._changeMgr;
            if (chMgr != null && !chMgr.isEmpty()) {
                if (!Immutable.is(curVal, newVal)) {
                    chMgr.next(newVal);
                }
            }
            return newVal;
        } finally {
            ReactiveContext.__reactiveListener = curScope;
        }
    }

    getNonReactive(): T {
        if (this._dirty || this._value == null) {
            return this.compute();
        } else {
            return this._value;
        }
    }

    get(): T {
        const val = this.getNonReactive();
        registerDep(this);
        return val;
    }

    subscribe(observer: IObserver<T>): ISubscription {
        if (this._changeMgr == null)
            this._changeMgr = new SubscriptionManager();
        return this._changeMgr.subscribe(observer);
    }

    subscribeInvalidated(observer: IObserver<null>): ISubscription {
        if (this._invalidationMgr == null)
            this._invalidationMgr = new SubscriptionManager();
        return this._invalidationMgr.subscribe(observer);
    }

    prop() {
        return null;
    }

    metadata(): IRef<Metadata> {
        return nullRef;
    }

    metadataStore(): IRef<MetadataMap> {
        return nullRef;
    }

    cleanCopy(): IRef<T> {
        return nullRef;
    }

    realized() {
        return this._value != null;
    }

    dirty(): IRefRO<boolean> {
        return nullRef;
    }

    childRO<C>(prop: IPropRO<T, C>): IRefRO<C> {
        return new RefRO(this, prop);
    }

    childRecordRO<C>(prop: IPropRecordRO<T, C>): IRefResolvedRO<C> {
        return new RefResolvedRO(prop.getField(this.getResolved()), this.childRO(prop));
    }

    getResolved(): T {
        return this.get();
    }

    getResolvedNonReactive(): T {
        return this.getNonReactive();
    }

    resolveRO() {
        return this;
    }

    _unregisterRefs() {
        const subs = this._subs;
        this._subs = [];
        subs.forEach((s) => s.unsubscribe());
    }

    equals(other: any) {
        return super.equals(other)
            && this._f === other._f;
    }

    dispose() {
        this._unregisterRefs();
        this._changeMgr && this._changeMgr.dispose();
        this._invalidationMgr && this._invalidationMgr.dispose();
        this._dirty = true;
    }
}

export function reaction<T>(f: () => T): Reaction<T> {
    return new Reaction(f);
}
