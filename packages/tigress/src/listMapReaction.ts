import { IRefRO } from './IRef';
import { listProp } from './Prop';
import { reaction } from './RefRO';
import {List} from "immutable";

export function listMapReaction<V, R>(cur: IRefRO<List<V>>, f: (cur: IRefRO<V>, idx: number) => R): IRefRO<R[]> {
    return reaction(function() {
        const list = cur.get();
        if (list == null) return [];
        return list.map(function(v: V, idx: number) {
            const iCur = cur.childRO(listProp<V>(idx));
            return f(iCur, idx);
        }).toArray();
    });
}
