import { IObserver } from "./IObserver";
import { ISubscription } from "./ISubscription";

export interface IObservable<T> {
    subscribe(observer: IObserver<T>): ISubscription;
}
