import { IEquals } from "./equals";

export interface IPropRO<P, C> extends IEquals {
    name: string;
    get(parent: P | null): C | null;
}

export interface IProp<P, C> extends IPropRO<P, C> {
    set(parent: P | null, value: C | null): P | null;
}

export interface IPropRecordRO<P, C> extends IPropRO<P, C> {
    getField(parent: P): C;
}

export interface IPropRecord<P, C> extends IPropRecordRO<P, C>, IProp<P, C> {
}
