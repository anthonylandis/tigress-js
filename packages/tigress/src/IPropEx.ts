import { IEquals } from "./equals";
import { IRef, IRefRO } from "./IRef";

export interface IPropExRO<P, C> extends IEquals {
    name: string;
    getExRO(ref: IRefRO<P>): IRefRO<C>;
}

export interface IPropEx<P, C> {
    getEx(ref: IRef<P>): IRef<C>;
}
