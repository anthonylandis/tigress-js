export interface IEquals {
    equals(other: any): boolean;
}

export class Equals {
    equals(other: any) {
        return other == null ? false : this.constructor === other.constructor;
    }
}
