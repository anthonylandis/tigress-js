import { IRefRO } from "./IRef";

export const ReactiveContext: {
    __reactiveListener?: (ref: IRefRO<any>) => void,
} = {};

export function registerDep<V>(ref: IRefRO<V>): void {
    const listener = ReactiveContext.__reactiveListener;
    if (listener) {
        listener(ref);
    }
}
