import { IObserver } from "./IObserver";
import { IProp, IPropRO } from "./IProp";
import { IRef } from "./IRef";
import { ISubscription } from "./ISubscription";

export function nullSubscribe(observer: IObserver<any>): ISubscription {
    let closed = false;
    const sub = {
        unsubscribe() {
            closed = true;
        },
        closed: () => closed,
    };
    if (observer.start != null) observer.start(sub);
    if (observer.complete != null) observer.complete(sub);
    return sub;
}

export const nullRef: IRef<any> = {
    get: () => null,
    getNonReactive: () => null,
    childRO: (_: IPropRO<any, any>) => nullRef,
    prop: () => null,
    resolveRO: () => null,
    realized: () => true,
    metadata: () => nullRef,
    metadataStore: () => nullRef,
    cleanCopy: () => nullRef,
    dirty: () => nullRef,
    subscribe: nullSubscribe,
    subscribeInvalidated: nullSubscribe,
    equals: (other) => other === nullRef,
    swap: (_) => null,
    reset: (_) => null,
    child: (_: IProp<any, any>) => nullRef,
    resolve: () => null,
};
