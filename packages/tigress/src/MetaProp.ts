import { Equals } from './equals';
import { Metadata } from './metadata';
import { IMetaProp } from './metadata';
import { IRefRO } from './IRef';
import { IPropExRO } from './IPropEx';

const _registeredMetaProps: { [key: string]: MetaProp<any> } = {};

export class MetaProp<T> extends Equals implements IMetaProp<T>, IPropExRO<Metadata, T> {
    name: string;

    constructor(name: string) {
        super();
        if (_registeredMetaProps[name]) {
            throw new Error("Property with name " + name + " already defined");
        }
        this.name = name;
        _registeredMetaProps[name] = this;
    }

    equals(other: any) {
        return super.equals(other)
            && this.name === other.name;
    }

    get(parent: Metadata): T | null {
        return parent.get(this);
    }

    set(parent: Metadata, value: T | null): Metadata {
        return parent.set(this, value);
    }

    getExRO(ref: IRefRO<Metadata>): IRefRO<T> {
        return ref.childRO(this);
    }
}

// export const metaProp : IPropExRO<any, Metadata> = {
//   name: '#meta',
//     getExRO: (ref: IRefRO<any>) => ref.metadata()
// };
