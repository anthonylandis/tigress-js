import * as Immutable from "immutable";
import { Equals } from "./equals";
import { IProp, IPropRecord, IPropRecordRO, IPropRO } from "./IProp";
import { IPropEx, IPropExRO } from "./IPropEx";
import { IRef, IRefRO } from "./IRef";

export class PropRO<S, A> extends Equals implements IPropRO<S, A>, IPropExRO<S, A> {
    protected _mgetter: (parent: S) => A | null;
    public name: string;

    constructor(mgetter: (parent: S) => A | null, name: string) {
        super();
        this._mgetter = mgetter;
        this.name = name;
    }

    get(parent: S): A | null {
        return this._mgetter(parent);
    }

    getExRO(ref: IRefRO<S>): IRefRO<A> {
        return ref.childRO(this);
    }

    equals(other: any) {
        return super.equals(other)
            && this._mgetter === other._mgetter
            && this.name === other.name;
    }
}

export class PropRecordRO<S, A> extends PropRO<S, A> implements IPropRecordRO<S, A> {
    _getter: (parent: S) => A;

    constructor(getter: (parent: S & {}) => A, name: string) {
        super((p) => p == null ? p : getter(p), name);
        this._getter = getter;
    }

    getField(parent: S): A {
        return this._getter(parent);
    }

    equals(other: any) {
        return super.equals(other)
            && this._getter === other._getter;
    }
}

export class Prop<S, A> extends PropRO<S, A> implements IProp<S, A>, IPropEx<S, A> {
    _setter: (parent: S, value: A | null) => S;

    constructor(mgetter: (parent: S) => A | null, setter: (parent: S | null, value: A | null) => S, name: string) {
        super(mgetter, name);
        this._setter = (p, v) => setter(p, v);
    }

    set(parent: S, value: A | null): S {
        return this._setter(parent, value);
    }

    getEx(ref: IRef<S>): IRef<A> {
        return ref.child(this);
    }

    equals(other: any) {
        return super.equals(other)
            && this._setter === other._setter;
    }
}

export class PropRecord<S, A> extends Prop<S, A> implements IPropRecord<S, A> {
    private _getField: (parent: S) => A;

    constructor(getter: (parent: S & {}) => A,
                setter: (parent: S & {}, value: A) => S,
                name: string) {
        super((p) => p == null ? p : getter(p), setter, name);
        this._getField = getter;
    }

    getField(parent: S): A {
        return this._getField(parent);
    }

    equals(other: any) {
        return super.equals(other)
            && this._getField === other._getField;
    }
}

export function notNullCase<T>(): IProp<T | null, T> {
    return new Prop(
        function(x: T | null): T | null { return x; },
        function(x: T | null, y: T | null): T | null { return y; },
        '.');
}

const __cleanProp: IPropExRO<any, any> = {
    name: '#clean',
    getExRO: (ref) => ref.cleanCopy(),
    equals: (other) => other === __cleanProp,
};

export function cleanProp<T>(): IPropExRO<T, T> {
    return __cleanProp;
}

export function objProp<V>(key: string): IProp<{ [key: string]: V; }, V> {
    return new Prop(
        function(p: { [key: string]: V; }) {
            return p[key];
        },
        function(p: { [key: string]: V; }, v: V | null) {
            const newP = { ...p };
            if (v == null) delete newP[key];
            else newP[key] = v;
            return newP;
        },
        //  function(p) {
        //   const newP = {...p};
        //   delete newP[key];
        //   return newP;
        // },
        key,
    );
}

export function listProp<V>(idx: number): Prop<Immutable.List<V>, V> {
    return new Prop(
        function(p: Immutable.List<V>): V | null {
            const res = p.get(idx);
            return res === undefined ? null : res;
        },
        function(p: Immutable.List<V>, v: V | null): Immutable.List<V> {
            return v == null ? p.delete(idx) : p.set(idx, v);
        },
        // p => p.delete(idx),
        idx.toString(),
    );
}

export function mapProp<K, V>(key: K, defaultNotFound: V | null): IProp<Immutable.Map<K, V>, V> {
    return new Prop(
        function(p: Immutable.Map<K, V>): V | null {
            const res = p.get(key);
            return res == null ? defaultNotFound : res;
        },
        function(p: Immutable.Map<K, V>, v: V | null): Immutable.Map<K, V> {
            return v == null ? p.delete(key) : p.set(key, v);
        },
        // p => p.delete(key),
        String(key));
}

export function setProp<T>(x: T): IPropRecord<Immutable.Set<T>, boolean> {
    return new PropRecord(
        function(p: Immutable.Set<T>) {
            return p.has(x);
        },
        function(p: Immutable.Set<T>, v: boolean) {
            return v ? p.add(x) : p.delete(x);
        },
        String(x)
    );
}

export function arrayProp<V>(idx: number): IPropRO<V[], V> {
    return new PropRO((p: V[]) => p[idx], idx.toString());
}

export function identityProp<T>(): IPropRecord<T | null, T | null> {
    return new PropRecord((x) => x, (x: T | null, y: T | null) => y, '.');
}
