import { ISubscription } from "./ISubscription";

export interface IObserver<T> {
    /** Receives the subscription object when `subscribe` is called */
    start?: (subscription: ISubscription) => void;
    /** Receives the next value in the sequence */
    next?: (value: T | null, sub: ISubscription) => void;
    /** Receives the sequence error */
    error?: (errorValue: any, sub: ISubscription) => void;
    /** Receives a completion notification */
    complete?: (sub: ISubscription) => void;
}
