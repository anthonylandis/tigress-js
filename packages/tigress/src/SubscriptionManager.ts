import { IObserver } from "./IObserver";
import { ISubscription } from "./ISubscription";

class SubscriptionImpl<T> implements ISubscription {
    _observer?: IObserver<T>;
    _unsubscribe?: (() => void);

    constructor(observer: IObserver<T>, unsub: () => void) {
        this._observer = observer;
        this._unsubscribe = unsub;
    }

    unsubscribe() {
        const unsub = this._unsubscribe;
        if (unsub != null) {
            unsub();
            this._observer = undefined;
            this._unsubscribe = undefined;
        }
    }

    closed() {
        return this._unsubscribe == null;
    }
}

let __subscriptionId = 0;

export class SubscriptionManager<T> {
    private _subscriptions: { [key: string]: SubscriptionImpl<T> };

    constructor() {
        this._subscriptions = {};
    }

    subscribe(observer: IObserver<T>): ISubscription {
        const id = (__subscriptionId++).toString();
        const This = this;
        const sub = new SubscriptionImpl(observer, () => {
            delete This._subscriptions[id];
            This._onUnsubscribe();
        });
        this._subscriptions[id] = sub;
        if (observer.start != null) observer.start(sub);
        return sub;
    }

    protected _onUnsubscribe(): void {
    }

    isEmpty(): boolean {
        return Object.keys(this._subscriptions).length === 0;
    }

    next(t: T | null): void {
        this._all((sub) => {
            const obs = sub._observer;
            if (obs != null && obs.next != null) {
                obs.next(t, sub);
            }
        });
    }

    error(err: any): void {
        this._all((sub) => {
            const obs = sub._observer;
            if (obs != null && obs.error != null) {
                obs.error(err, sub);
            }
        });
    }

    complete(): void {
        this._all((sub) => {
            const obs = sub._observer;
            if (obs != null && obs.complete != null) {
                obs.complete(sub);
            }
        });
    }

    dispose(): void {
        this._all((sub) => sub.unsubscribe());
        this._subscriptions = {};
    }

    private _all(f: (t: SubscriptionImpl<T>) => void) {
        const subs = this._subscriptions;
        Object.keys(subs).forEach(function(id: string) {
            const sub = subs[id];
            if (sub != null) {
                f(sub);
            }
        });
    }
}
