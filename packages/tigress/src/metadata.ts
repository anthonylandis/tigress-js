import * as Immutable from "immutable";
import { IProp } from "./IProp";

export interface IMetaProp<T> extends IProp<Metadata, T> {
    name: string;

    get(parent: Metadata): T | null;

    set(parent: Metadata, value: T | null): Metadata;
}

export class Metadata {
    _val: Immutable.Map<string, any>;

    constructor(val?: Immutable.Map<string, any>) {
        this._val = val ? val : Immutable.Map<string, any>();
    }

    get<T>(prop: IMetaProp<T>): T | null {
        return this._val.get(prop.name) as T | null;
    }

    set<T>(prop: IMetaProp<T>, val: T): Metadata {
        return new Metadata(this._val.set(prop.name, val));
    }

    clear<T>(prop: IMetaProp<T>): Metadata {
        return this.set(prop, null);
    }

    all(): Immutable.Map<string, any> {
        return this._val;
    }
}

export type MetadataMap = {
    metadata?: Metadata;
    branches?: Immutable.Map<string, MetadataMap>;
};
