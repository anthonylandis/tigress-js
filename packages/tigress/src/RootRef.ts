import { Equals } from "./equals";
import { IObserver } from "./IObserver";
import { IProp, IPropRecord, IPropRecordRO, IPropRO } from "./IProp";
import { IRef, IRefResolved, IRefResolvedRO, IRefRO } from "./IRef";
import { ISubscription } from "./ISubscription";

import { MetadataMap, Metadata } from "./metadata";
import { nullRef } from "./nullRef";
import { Ref, RefResolved } from "./Ref";
import { _dirtyReaction, _metadata, RefResolvedRO, RefRO } from "./RefRO";
import { registerDep } from "./registerDep";
import { SubscriptionManager } from "./SubscriptionManager";

// type RootState<T> = {
//     root: T;
//     cleanRoot?: T;
//     metadataStore?: MetadataMap;
// };

export class StateStore<T extends {}> implements IRefResolved<T> {
    _state: T;
    _subMgr: SubscriptionManager<T>;

    constructor(init: T) {
        if (init == null)
            throw new Error("Null or undefined is an invalid value for a root ref");
        this._state = init;
        this._subMgr = new SubscriptionManager();
    }

    getNonReactive() {
        return this.getResolvedNonReactive();
    }

    get() {
        return this.getResolved();
    }

    getResolvedNonReactive(): T {
        return this._state;
    }

    getResolved(): T {
        registerDep(this);
        return this.getResolvedNonReactive();
    }

    realized(): boolean {
        return true;
    }

    subscribe(observer: IObserver<T>): ISubscription {
        return this._subMgr.subscribe(observer);
    }

    subscribeInvalidated(observer: IObserver<any>): ISubscription {
        return this._subMgr.subscribe(observer);
    }

    swap(updateFn: (curVal: T | null) => T | null): T | null {
        const st = this._state;
        const newSt = updateFn(st);
        if (newSt == null)
            throw new Error("Null or undefined is an invalid value for a root ref");
        this._state = newSt;
        this._subMgr.next(newSt);
        return newSt;
    }

    swapResolved(updateFn: (curVal: T) => T): T {
        const st = this._state;
        const newSt = updateFn(st);
        if (newSt == null)
            throw new Error("Null or undefined is an invalid value for a root ref");
        this._state = newSt;
        this._subMgr.next(newSt);
        return newSt;
    }

    reset(newVal: T) {
        return this.swapResolved((_) => newVal);
    }

    childRO<V>(prop: IPropRO<T, V>): IRefRO<V> {
        return new RefRO(this, prop);
    }

    child<V>(prop: IProp<T, V>): IRef<V> {
        return new Ref(this, prop);
    }

    childRecordRO<A>(prop: IPropRecordRO<T, A>): IRefResolvedRO<A> {
        return new RefResolvedRO(prop.getField(this._state), this.childRO(prop));
    }

    childRecord<A>(prop: IPropRecord<T, A>): IRefResolved<A> {
        return new RefResolved(prop.getField(this._state), this.child(prop));
    }

    resolveRO() {
        return this;
    }

    resolve() {
        return this;
    }

    prop() {
        return null;
    }

    metadata(): IRef<Metadata> {
        return nullRef;
    }

    metadataStore(): IRef<MetadataMap> {
        return nullRef;
    }

    dirty(): IRefRO<boolean> {
        return nullRef;
    }

    cleanCopy(): IRefRO<T> {
        return nullRef;
    }

    equals(other: any) {
        return other === this;
    }
}

class RootRef<T extends {}> extends Equals implements IRefResolved<T> {
    _rootRef: IRefResolved<T>;
    _metadataRef?: IRef<Metadata>;
    _metadataStoreRef: IRef<MetadataMap>;
    _cleanCopyRef: IRefRO<T>;

    constructor(rootRef: IRefResolved<T>,
        cleanCopyRef?: IRefRO<T> | null,
        metadataStoreRef?: IRef<MetadataMap> | null) {
        super();
        this._rootRef = rootRef;
        this._cleanCopyRef = cleanCopyRef == null ? nullRef : cleanCopyRef;
        this._metadataStoreRef = metadataStoreRef == null ? nullRef : metadataStoreRef;
    }

    getResolved(): T {
        registerDep(this);
        return this.getResolvedNonReactive();
    }

    get() {
        registerDep(this);
        return this.getNonReactive();
    }

    getResolvedNonReactive(): T {
        return this._rootRef.getResolvedNonReactive();
    }

    getNonReactive() {
        return this._rootRef.getNonReactive();
    }

    realized(): boolean {
        return true;
    }

    subscribe(observer: IObserver<T>): ISubscription {
        return this._rootRef.subscribe(observer);
    }

    subscribeInvalidated(observer: IObserver<any>): ISubscription {
        return this._rootRef.subscribeInvalidated(observer);
    }

    swap(updateFn: (curVal: T | null) => T | null): T | null {
        return this._rootRef.swap(updateFn);
    }

    swapResolved(updateFn: (x: T) => T): T {
        return this._rootRef.swapResolved(updateFn);
    }

    reset(newVal: T) {
        return this._rootRef.reset(newVal);
    }

    childRO<V>(prop: IPropRO<T, V>): IRefRO<V> {
        return new RefRO(this, prop);
    }

    child<V>(prop: IProp<T, V>): IRef<V> {
        return new Ref(this, prop);
    }

    childRecordRO<V>(prop: IPropRecordRO<T, V>): IRefResolvedRO<V> {
        return new RefResolvedRO(prop.getField(this.getResolvedNonReactive()), this.childRO(prop));
    }

    childRecord<A>(prop: IPropRecord<T, A>): IRefResolved<A> {
        return new RefResolved(prop.getField(this.getResolvedNonReactive()), this.child(prop));
    }

    resolveRO() {
        return this;
    }

    resolve() {
        return this;
    }

    prop() {
        return null;
    }

    metadata(): IRef<Metadata> {
        let metaRef = this._metadataRef;
        if (metaRef != null) return metaRef;
        metaRef = _metadata(this._metadataStoreRef);
        this._metadataRef = metaRef;
        return metaRef;
    }

    metadataStore(): IRef<MetadataMap> {
        return this._metadataStoreRef;
    }

    dirty(): IRefRO<boolean> {
        return _dirtyReaction(this._rootRef, this._cleanCopyRef);
    }

    cleanCopy(): IRefRO<T> {
        return this._cleanCopyRef;
    }

    equals(other: any) {
        return super.equals(other) &&
            this._rootRef.equals(other._rootRef) &&
            this._cleanCopyRef.equals(other._cleanCopyRef) &&
            this._metadataStoreRef.equals(other._metadataStoreRef);
    }
}

export function rootRef<T extends {}>(initState: T, cleanView?: IRefRO<T> | null): IRefResolved<T> {
    return new RootRef(new StateStore(initState), cleanView, new StateStore({}));
}
