import * as Immutable from 'immutable';
import { identityProp, IObserver, IRefResolved, IRefResolvedRO } from '../src/tigress';
import { listMapReaction, reaction, rootRef, setProp } from '../src/tigress';

type TestEventType = "start" | "complete" | "error" | "next";

type TestEventLogEntry<T> = [TestEventType, T | null];

type TestEventLog<T> = Array<[TestEventType, T | null]>;

const start: TestEventLogEntry<any> = ['start', null];
const complete: TestEventLogEntry<any> = ['start', null];
const error: TestEventLogEntry<any> = ['start', null];

function testObserver<T>(eventLog: TestEventLog<T>): IObserver<T> {
    return {
        start: () => { eventLog.push(start); },
        complete: () => { eventLog.push(complete); },
        next: (x) => { eventLog.push(['next', x]); },
        error: () => { eventLog.push(error); },
    };
}

const refEquals = (ref, expected) => {
    expect(ref.get()).toEqual(expected);
    expect(ref.getNonReactive()).toEqual(expected);
};

const refResolvedEquals = (ref, expected) => {
    refEquals(ref, expected);
    expect(ref.getResolved()).toEqual(expected);
    expect(ref.getResolvedNonReactive()).toEqual(expected);
};

function expectGotEvents<T>(eventLog: TestEventLog<T>, expectedEvents: TestEventLog<T>) {
    expect(eventLog).toEqual(expectedEvents);
    // clear all events
    while (eventLog.length > 0) {
        eventLog.pop();
    }
}

test("rootRef's are sane and events work", () => {
    const eventLog: TestEventLog<number> = [];
    const r0: IRefResolved<number> = rootRef(0);
    r0.subscribe(testObserver(eventLog));
    refResolvedEquals(r0, 0);
    r0.reset(1);
    expectGotEvents(eventLog, [start, ['next', 1]]);
    refResolvedEquals(r0, 1);
    r0.swap((x) => x == null ? null : x + 1);
    expectGotEvents(eventLog, [['next', 2]]);
    refResolvedEquals(r0, 2);
});

test("identityProp and child RefRO's are sane", () => {
    const r0: IRefResolved<number> = rootRef(0);
    const r1 = r0.childRO(identityProp());
    refEquals(r1, 0);
    r0.reset(1);
    refEquals(r1, 1);
    r0.swap((x) => x == null ? null : x + 1);
    refEquals(r1, 2);
});

test("identityProp and child Ref's are sane", () => {
    const r0: IRefResolved<number | null> = rootRef(0);
    const r1 = r0.child(identityProp<number | null>());
    refEquals(r1, 0);
    r0.reset(1);
    refEquals(r1, 1);
    r0.swap((x) => x == null ? null : x + 1);
    refEquals(r1, 2);
});

const increment = (x) => x == null ? null : x + 1;

test("identityProp and child RefResolvedRO's are sane", () => {
    const r0: IRefResolved<number> = rootRef(0);
    const r1 = r0.childRecordRO(identityProp());
    refResolvedEquals(r1, 0);
    r0.reset(1);
    refResolvedEquals(r1, 1);
    r0.swap(increment);
    refResolvedEquals(r1, 2);
});

test("reactions and invalidations work", () => {
    const invalidationLog: TestEventLog<any> = [];
    const r0: IRefResolved<number> = rootRef(1);
    const r1: IRefResolved<number> = rootRef(2);
    const r2: IRefResolvedRO<number> = reaction(() => r0.getResolved() + r1.getResolved());
    r2.subscribeInvalidated(testObserver(invalidationLog));
    refResolvedEquals(r2, 3);
    r0.swap(increment);
    expectGotEvents(invalidationLog, [start, ['next', null]]);
    refResolvedEquals(r2, 4);
    r1.swap(increment);
    expectGotEvents(invalidationLog, [['next', null]]);
    refResolvedEquals(r2, 5);
});

test("reactions and changes work", () => {
    const invalidationLog: TestEventLog<any> = [];
    const changeLog: TestEventLog<number> = [];
    const r0: IRefResolved<number> = rootRef(1);
    const r1: IRefResolved<number> = rootRef(2);
    const r2: IRefResolvedRO<number> = reaction(() => r0.getResolved() + r1.getResolved());
    r2.subscribeInvalidated(testObserver(invalidationLog));
    r2.subscribe(testObserver(changeLog));
    expectGotEvents(invalidationLog, [start]);
    expectGotEvents(changeLog, [start]);
    refResolvedEquals(r2, 3);
    expectGotEvents(invalidationLog, []);
    expectGotEvents(changeLog, [['next', 3]]);
    r0.swap(increment);
    expectGotEvents(invalidationLog, [['next', null]]);
    expectGotEvents(changeLog, [['next', 4]]);
    refResolvedEquals(r2, 4);
    r1.swap(increment);
    expectGotEvents(invalidationLog, [['next', null]]);
    expectGotEvents(changeLog, [['next', 5]]);
    refResolvedEquals(r2, 5);
    expectGotEvents(invalidationLog, []);
    expectGotEvents(changeLog, []);
});

test("listMapReaction works", () => {
    const r0: IRefResolved<Immutable.List<number>> = rootRef(Immutable.List([0, 1, 2]));
    const r1 = listMapReaction(r0, (r) => r.get());
    refResolvedEquals(r1, [0, 1, 2]);
    r0.swap((l) => l == null ? null : l.push(3));
    refResolvedEquals(r1, [0, 1, 2, 3]);
});

test("setProp works", () => {
    const r0: IRefResolved<Immutable.Set<number>> = rootRef(Immutable.Set.of(1));
    const r1 = r0.childRecord(setProp(1));
    expect(r1.getResolved()).toBeTruthy();
    r1.reset(false);
    expect(r1.getResolved()).toBeFalsy();
    expect(r0.getResolved().has(1)).toBeFalsy();
});
